# Weether

### Contents

- [Installing dependencies](#installing-dependencies)
- [Running the app](#running-the-app)
- [Test](#test)
- [Migration](#migration)
- [Architecture](#architecture)
    - [Use Case](#use-case)
    - [Adapter](#adapter)
- [External documentation](#external-documentation)

## Installing dependencies

```bash
$ npm install
or
$ npm i
```

## Running the app

```bash
# development
$ npm run start

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Migration

Whenever a change is made to a model entity (column, table name, etc.) a new migration should be arranged.
Each migration will mean a new db version.

**IMPORTANT**: all model file names should end with `*.model.ts`

```bash
# generate migration file
$ npm run migration:generate -- -n NameOfMigration

# apply migration to db
$ npm run migration:run
```

## Architecture

### Use case

- Query

```typescript
@Injectable()
export class FindPetQry implements Query<Pet, Id> {
  constructor(private readonly petRepository: PetRepository) {}

  run(id: Id): Promise<Pet> {
    return this.petRepository.findById(id);
  }
}
```

- Command

```typescript
@Injectable()
export class SavePetCmd implements Command<Pet> {
  constructor(private readonly petRepository: PetRepository) {}

  run(pet: Pet): Promise<void> {
    return this.petRepository.insert(pet);
  }
}
```

### Adapter
We have a decorator named `Mapper` that will help us with the boilerplate. 
When using this decorator, it's important to put this line at the top of the file to avoid 
lint warnings:
```typescript
/* eslint-disable @typescript-eslint/no-unused-vars */
``` 

- When same property names

```typescript
@Injectable()
export class PetAdapter implements Adapter<Pet, PetDto> {
  @Mapper()
  toDto(model: Pet): PetDto {
    return;
  }

  @Mapper()
  toModel(modelDto: PetDto): Pet {
    return;
  }
}
```

- When different property names

The mapper decorator is typed, and will only allow property names from the respective model.
We only have to specify the property names that change between models, ignoring the ones that have the same name. 

```typescript
@Injectable()
export class PetAdapter implements Adapter<Pet, PetDto> {
  @Mapper<Pet, PetDto>({ bornIn: 'birthDate', height: 'petHeight' })
  toDto(model: Pet): PetDto {
    return;
  }

  @Mapper<PetDto, Pet>({ birthDate: 'bornIn', petHeight: 'height' })
  toModel(modelDto: PetDto): Pet {
    return;
  }
}
```

- When modified property values

```typescript
@Injectable()
export class PetAdapter implements Adapter<Pet, PetDto> {
  toDto(model: Pet): PetDto {
    return {
      name: model.name,
      bornIn: model.bornIn.toISOString(),
    };
  }

  toModel(modelDto: PetDto): Pet {
    return {
      name: modelDto.name,
      bornIn: Date.parse(modelDto.bornIn),
    };
  }
}
```

## External documentation

- [Nestjs](https://docs.nestjs.com/)
- [TypeORM](https://typeorm.io/#/)
