import * as fs from 'fs';
import { ConfigService } from '../../config/application/config.service';
import { Environment } from '../../config/domain/environment';
import * as migrationConfig from '../migration.config.json';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const config: TypeOrmModuleOptions = {
  type: 'postgres',
  host: ConfigService.getConfig().db.host,
  port: ConfigService.getConfig().db.port,
  password: ConfigService.getConfig().db.password,
  database: ConfigService.getConfig().db.name,
  username: ConfigService.getConfig().db.username,
  ssl: ConfigService.getEnvironment() === Environment.PRO,
  ...migrationConfig,
};

fs.writeFileSync('ormconfig.json', JSON.stringify(config, null, 2));
