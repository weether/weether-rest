import { MigrationInterface, QueryRunner } from 'typeorm';

export class createSnapshotsTable1597848012874 implements MigrationInterface {
  name = 'createSnapshotsTable1597848012874';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "snapshots" ("dateTime" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP, "temperature" numeric NOT NULL, "airHumidity" numeric NOT NULL, "soilHumidity" numeric NOT NULL, "soilPH" numeric NOT NULL, "co2" numeric NOT NULL, CONSTRAINT "PK_cb443185e14408f7657eaab76d3" PRIMARY KEY ("dateTime"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "snapshots"`);
  }
}
