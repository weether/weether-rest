import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUsersTable1597518383095 implements MigrationInterface {
  name = 'createUsersTable1597518383095';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "users" ("username" character varying(50) NOT NULL, "password" character varying(300) NOT NULL, CONSTRAINT "PK_fe0bb3f6520ee0469504521e710" PRIMARY KEY ("username"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "users"`);
  }
}
