import { Module } from '@nestjs/common';
import { ConfigService } from 'src/config/application/config.service';
import { createConnection } from 'typeorm';
import { Environment } from 'src/config/domain/environment';
import * as migrationConfig from './migration.config.json';
import { Types } from 'src/types';

@Module({
  providers: [DbModule.DATABASE_PROVIDER],
  exports: [DbModule.DATABASE_PROVIDER],
})
export class DbModule {
  static DATABASE_PROVIDER = {
    provide: Types.DATABASE_CONNECTION,
    useFactory: async () =>
      await createConnection({
        type: 'postgres',
        host: ConfigService.getConfig().db.host,
        port: ConfigService.getConfig().db.port,
        password: ConfigService.getConfig().db.password,
        database: ConfigService.getConfig().db.name,
        username: ConfigService.getConfig().db.username,
        ssl: ConfigService.getEnvironment() === Environment.PRO,
        ...migrationConfig,
      }),
  }
}