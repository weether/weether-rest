export interface DbConfig {
  host: string;
  port: number;
  password: string;
  name: string;
  username: string;
}
