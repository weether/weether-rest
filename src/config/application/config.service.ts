import { Config } from '../domain/config';
import { Environment } from '../domain/environment';
import { config } from '../config';
import { EnvironmentDoesNotExistException } from '../errors/environment-does-not-exist-exception';
import { Logger } from '@nestjs/common';

export class ConfigService {
  private readonly config: Config;
  private environment: Environment;
  private static instance: ConfigService;

  constructor() {
    this.setEnvironment();
    this.logEnvironment();
    this.config = config.get(this.environment);
  }

  static getConfig(): Config {
    if (ConfigService.instance === undefined) {
      ConfigService.instance = new ConfigService();
    }
    return ConfigService.instance.config;
  }

  static getEnvironment(): Environment {
    if (ConfigService.instance === undefined) {
      ConfigService.instance = new ConfigService();
    }
    return ConfigService.instance.environment;
  }

  private setEnvironment() {
    const env = process.env.ENVIRONMENT || 'dev';
    this.environment = Environment[env.toUpperCase()];

    if (this.environment === undefined) {
      throw new EnvironmentDoesNotExistException(env);
    }
  }

  private logEnvironment() {
    let printEnvironmentFont = '';
    if (this.environment === Environment.DEV) {
      printEnvironmentFont = `
               ____  _____ _____
        ---   |    \\|   __|  |  |  ---
     ------   |  |  |   __|  |  |  ------
  ---------   |____/|_____|\\___/   ---------
  `;
    } else if (this.environment === Environment.PRO_DOCKER) {
      printEnvironmentFont = `                                              
             _____  _____  _____       ____   _____  _____  _____  _____  _____ 
      ---   |  _  || __  ||     | ___ |    \\ |     ||     ||  |  ||   __|| __  |   ---
   ------   |   __||    -||  |  ||___||  |  ||  |  ||   --||    -||   __||    -|   ------   
---------   |__|   |__|__||_____|     |____/ |_____||_____||__|__||_____||__|__|   ---------
`;
    } else if (this.environment === Environment.PRO) {
      printEnvironmentFont = `
               _____ _____ _____
        ---   |  _  | __  |     |   ---
     ------   |   __|    -|  |  |   ------
  ---------   |__|  |__|__|_____|   ---------
`;
    }
    Logger.debug(
      '\n------------   Setting environment   -------------\n' +
        printEnvironmentFont,
    );
  }
}
