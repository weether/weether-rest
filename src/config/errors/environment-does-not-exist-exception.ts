import { Environment } from '../domain/environment';

export class EnvironmentDoesNotExistException extends Error {
  constructor(env: string) {
    super(
      `Environment ${env} does not exist. Existing environments: ${Object.values(
        Environment,
      )}`,
    );
  }
}
