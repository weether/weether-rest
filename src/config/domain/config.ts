import { DbConfig } from '../../db/domain/db-config';

export interface Config {
  db: DbConfig;
}
