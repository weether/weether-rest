import { Environment } from './domain/environment';
import { Config } from './domain/config';

export const config = new Map<Environment, Config>([
  [
    Environment.DEV,
    {
      db: {
        host: '127.0.0.1',
        port: 5432,
        name: 'weether',
        password: 'verysecret',
        username: 'postgres',
      },
    },
  ],
  [
    Environment.PRO,
    {
      db: {
        host: '127.0.0.1',
        port: 5432,
        name: 'weether',
        password: 'verysecret',
        username: 'postgres',
      },
    },
  ],
  [
    Environment.PRO_DOCKER,
    {
      db: {
        host: '127.0.0.1',
        port: 5432,
        name: 'weether',
        password: 'verysecret',
        username: 'postgres',
      },
    },
  ],
]);
