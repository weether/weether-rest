import { Injectable } from '@nestjs/common';
import { User } from '../domain/user.model';
import { UserRepository } from '../domain/user.repository';
import { TypeOrmMapper } from '../../../core/types/typeorm-mapper';

@Injectable()
export class UserOrmRepository implements UserRepository {
  constructor(private readonly userORM: TypeOrmMapper<User>) {}

  find(username: string): Promise<User> {
    return this.userORM.findOne({ username });
  }

  async save(user: User): Promise<void> {
    await this.userORM.save(user);
  }
}
