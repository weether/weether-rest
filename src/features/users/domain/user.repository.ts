import { Finder } from '../../../core/types/finder';
import { User } from './user.model';
import { Saver } from '../../../core/types/saver';

export interface UserRepository extends Finder<User, string>, Saver<User> {}
