import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryColumn({ type: 'varchar', length: 50 })
  username: string;

  @Column({ type: 'varchar', length: 300 })
  password: string;
}
