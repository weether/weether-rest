import { anything, mock, verify, when } from 'ts-mockito';
import { UserOrmRepository } from '../infraestructure/user.orm-repository';
import { UserMother } from '../domain/user-mother';
import { SaveUserCmd } from './save-user-cmd';

describe('SaveUserCmd', () => {
  it('should call user repository', async () => {
    const { sut, userRepository } = setup();
    const user = UserMother.landown();
    when(userRepository.save(user)).thenReturn(Promise.resolve());

    await sut.run(user);

    verify(userRepository.save(anything())).once();
  });
});

function setup() {
  const userRepository = mock<UserOrmRepository>();

  return {
    sut: new SaveUserCmd(userRepository),
    userRepository,
  };
}
