import { Injectable } from '@nestjs/common';
import { Command } from '../../../core/types/use-case/command';
import { User } from '../domain/user.model';
import { UserOrmRepository } from '../infraestructure/user.orm-repository';
import * as argon2 from 'argon2';

@Injectable()
export class SaveUserCmd implements Command<User> {
  constructor(private readonly userRepository: UserOrmRepository) {}

  async run(user: User): Promise<void> {
    const password = await argon2.hash(user.password);
    return this.userRepository.save({ username: user.username, password });
  }
}
