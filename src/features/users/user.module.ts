import { Module } from '@nestjs/common';
import { JwtService } from '../auth/application/jwt.service';
import { UserController } from './controllers/user.controller';
import { SaveUserCmd } from './application/save-user-cmd';
import { UserAdapter } from './controllers/user-adapter';
import { UserOrmRepository } from './infraestructure/user.orm-repository';

@Module({
  controllers: [UserController],
  providers: [SaveUserCmd, JwtService, UserAdapter, UserOrmRepository],
})
export class UserModule {}
