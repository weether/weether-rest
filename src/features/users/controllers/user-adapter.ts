/* eslint-disable @typescript-eslint/no-unused-vars */
import { Adapter } from '../../../core/types/adapter';
import { User } from '../domain/user.model';
import { UserDto } from './user-dto';
import { Mapper } from '../../../core/decorators/mapper.decorator';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserAdapter implements Adapter<User, UserDto> {
  @Mapper()
  toDto(user: User): UserDto {
    return;
  }

  @Mapper()
  toModel(userDto: UserDto): User {
    return;
  }
}
