import { Body, Controller, Post } from '@nestjs/common';
import { UserDto } from './user-dto';
import { UserAdapter } from './user-adapter';
import { User } from '../domain/user.model';
import { SaveUserCmd } from '../application/save-user-cmd';

@Controller('users')
export class UserController {
  constructor(
    private readonly saveUserCmd: SaveUserCmd,
    private readonly userAdapter: UserAdapter,
  ) {}

  @Post()
  async postUser(@Body() user: UserDto) {
    return await this.saveUserCmd.run(this.userAdapter.toModel(user));
  }

  @Post()
  async signUp(@Body() user: User) {
    return await this.saveUserCmd.run(user);
  }
}
