import { Injectable } from '@nestjs/common';
import { Command } from 'src/core/types/use-case/command';
import { Snapshot } from '../domain/snapshot.model';
import { SnapshotTypeOrmRepository } from '../infraestructure/snapshot.type-orm-repository';

@Injectable()
export class SaveSnapshotCmd implements Command<Snapshot> {
  constructor(private readonly snapshotRepository: SnapshotTypeOrmRepository) {}

  async run(snapshot: Snapshot): Promise<void> {
    return this.snapshotRepository.save(snapshot);
  }
}
