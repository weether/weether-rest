import { SaveSnapshotCmd } from './save-snapshot-cmd';
import { deepEqual, instance, mock, verify } from 'ts-mockito';
import { SnapshotTypeOrmRepository } from '../infraestructure/snapshot.type-orm-repository';
import { SnapshotMother } from '../domain/snapshot-mother';

describe('SaveSnapshotCmd', () => {
  it('should call snapshot repository', async () => {
    const { sut, snapshotRepository } = setup();

    await sut.run(SnapshotMother.snapshot());

    verify(snapshotRepository.save(deepEqual(SnapshotMother.snapshot()))).once();
  });
});

function setup() {
  const snapshotRepository = mock(SnapshotTypeOrmRepository);

  return {
    sut: new SaveSnapshotCmd(instance(snapshotRepository)),
    snapshotRepository,
  };
}
