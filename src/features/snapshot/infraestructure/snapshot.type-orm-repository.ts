import { Inject, Injectable } from '@nestjs/common';
import { Snapshot } from '../domain/snapshot.model';
import { TypeOrmMapper } from '../../../core/types/typeorm-mapper';
import { SnapshotRepository } from '../domain/snapshot.repository';
import { Types } from '../../../types';

@Injectable()
export class SnapshotTypeOrmRepository implements SnapshotRepository {
  constructor(
    @Inject(Types.SNAPSHOT_REPOSITORY)
    private snapshotOrm: TypeOrmMapper<Snapshot>,
  ) {}
  async save(snapshot: Snapshot): Promise<void> {
    await this.snapshotOrm.save(snapshot);
  }
}
