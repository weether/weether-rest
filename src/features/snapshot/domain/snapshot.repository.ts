import { Saver } from '../../../core/types/saver';
import { Snapshot } from './snapshot.model';

// TODO: Delete comment when finder is implemented

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface SnapshotRepository extends Saver<Snapshot> {} 
