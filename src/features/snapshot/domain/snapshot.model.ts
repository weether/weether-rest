import { Column, PrimaryColumn, Entity } from 'typeorm';

@Entity('snapshots')
export abstract class Snapshot {
  @PrimaryColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  dateTime: Date;

  @Column('numeric')
  temperature: number;

  @Column('numeric')
  airHumidity: number;

  @Column('numeric')
  soilHumidity: number;

  @Column('numeric')
  soilPH: number;

  @Column('numeric')
  co2: number;
}
