import { Snapshot } from './snapshot.model';

export class SnapshotMother {
  static snapshot(): Snapshot {
    return {
      dateTime: new Date('1 January, 2020, 00:03:45 UTC'),
      temperature: 58,
      airHumidity: 13,
      soilHumidity: 32,
      soilPH: 14,
      co2: 11,
    };
  }
}
