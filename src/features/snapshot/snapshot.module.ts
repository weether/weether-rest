import { Module } from '@nestjs/common';
import { SnapshotTypeOrmRepository } from './infraestructure/snapshot.type-orm-repository';
import { DbModule } from 'src/db/db.module';
import { SnapshotController } from './controllers/snapshot.controller';
import { SnapshotAdapter } from './controllers/snapshot-adapter';
import { SaveSnapshotCmd } from './application/save-snapshot-cmd';
import { Connection } from 'typeorm';
import { Snapshot } from './domain/snapshot.model';
import { Types } from 'src/types';

@Module({
  imports: [DbModule],
  providers: [
    {
      provide: Types.SNAPSHOT_REPOSITORY,
      useFactory: (connection: Connection) =>
        connection.getRepository(Snapshot),
      inject: [Types.DATABASE_CONNECTION],
    },
    SnapshotTypeOrmRepository,
    SnapshotAdapter,
    SaveSnapshotCmd,
  ],
  controllers: [SnapshotController],
})
export class SnapshotModule {}
