import { IsNumber } from 'class-validator';

export class SnapshotDto {
  dateTime: Date;

  @IsNumber()
  temperature: number;

  @IsNumber()
  airHumidity: number;

  @IsNumber()
  soilHumidity: number;

  @IsNumber()
  soilPH: number;

  @IsNumber()
  co2: number;
}
