import { Body, Controller, Post } from '@nestjs/common';
import { SnapshotAdapter } from './snapshot-adapter';
import { SnapshotDto } from './snapshot-dto';
import { SaveSnapshotCmd } from '../application/save-snapshot-cmd';

@Controller('snapshots')
export class SnapshotController {
  constructor(
    private readonly saveSnapshotCmd: SaveSnapshotCmd,
    private readonly snapshotAdapter: SnapshotAdapter,
  ) {}

  @Post()
  async saveSnapshot(@Body() snapshot: SnapshotDto) {
    return await this.saveSnapshotCmd.run(this.snapshotAdapter.toModel(snapshot));
  }
}
