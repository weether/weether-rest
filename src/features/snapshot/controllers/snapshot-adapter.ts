/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable } from "@nestjs/common";
import { Snapshot } from "../domain/snapshot.model";
import { SnapshotDto } from "./snapshot-dto";
import { Mapper } from "../../../core/decorators/mapper.decorator";
import { Adapter } from "../../../core/types/adapter";

@Injectable ()
export class SnapshotAdapter implements Adapter <Snapshot, SnapshotDto> {
    @Mapper ()
    toDto(snapshot : Snapshot): SnapshotDto {
        return;
    }

    @Mapper ()
    toModel(snapshotDto: SnapshotDto): Snapshot {
        return;
    }
}
