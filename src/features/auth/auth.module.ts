import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { LoginCmd } from './application/login-cmd';
import { JwtService } from './application/jwt.service';

@Module({
  controllers: [AuthController],
  providers: [LoginCmd, JwtService],
})
export class AuthModule {}
