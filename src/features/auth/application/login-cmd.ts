import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AccessTokens } from '../domain/access-tokens';
import { Query } from '../../../core/types/use-case/query';
import { User } from '../../users/domain/user.model';
import { JwtService } from './jwt.service';
import { UserOrmRepository } from '../../users/infraestructure/user.orm-repository';
import * as argon2 from 'argon2';

@Injectable()
export class LoginCmd implements Query<AccessTokens, User> {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userRepository: UserOrmRepository,
  ) {}

  async run(user: User): Promise<AccessTokens> {
    const storedUser = await this.userRepository.find(user.username);

    if (storedUser === undefined) throw new UnauthorizedException();

    const passwordsMatch = await argon2.verify(
      storedUser.password,
      user.password,
    );

    if (!passwordsMatch) throw new UnauthorizedException();

    return this.jwtService.getAccessTokens(user.username);
  }
}
