import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { isToken, Token } from '../domain/token';
import { JwtVerifyKeys } from '../domain/jwt-verify-keys';
import { EncodedToken } from '../domain/encoded-token';
import { AccessTokens } from '../domain/access-tokens';

@Injectable()
export class JwtService {
  getAccessTokens(username: string): AccessTokens {
    const refreshToken = this.getRefreshToken(username);
    const appToken = this.getAppToken(refreshToken);
    return { refreshToken, appToken };
  }

  verifyAppToken(encodedToken: EncodedToken): Token {
    return this.tryTokenVerification<Token>(() => {
      const token = jwt.verify(
        encodedToken,
        JwtVerifyKeys.APP_TOKEN.toString(),
      );

      if (isToken(token)) {
        return token;
      }
    });
  }

  getAppToken(refreshEncodedToken: EncodedToken) {
    return this.tryTokenVerification<EncodedToken>(() => {
      const token = this.verifyRefreshToken(refreshEncodedToken);
      const expiresIn = 600;

      return jwt.sign(
        { username: token.username, expiresIn },
        JwtVerifyKeys.APP_TOKEN.toString(),
        { expiresIn },
      );
    });
  }

  private verifyRefreshToken(encodedToken: EncodedToken): Token {
    return this.tryTokenVerification<Token>(() => {
      const token = jwt.verify(
        encodedToken,
        JwtVerifyKeys.REFRESH_TOKEN.toString(),
      );

      if (isToken(token)) {
        return token;
      }
    });
  }

  private getRefreshToken(username: string) {
    return jwt.sign({ username }, JwtVerifyKeys.REFRESH_TOKEN.toString());
  }

  private tryTokenVerification<T>(callback: () => T): T {
    return callback();
  }
}
