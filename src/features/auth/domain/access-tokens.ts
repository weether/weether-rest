import { EncodedToken } from './encoded-token';

export type AccessTokens = {
  refreshToken: EncodedToken;
  appToken: EncodedToken;
};
