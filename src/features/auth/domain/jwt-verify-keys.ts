import * as config from '../jwt.config.json';

// TODO: Check how to solve this error
export enum JwtVerifyKeys {
  APP_TOKEN = (config['app-token-verify-key'] as unknown) as JwtVerifyKeys,
  REFRESH_TOKEN = (config[
    'refresh-token-verify-key'
  ] as unknown) as JwtVerifyKeys,
}
