export class Token {
  username: string;
  expiresIn: number;
}

export function isToken(value: unknown | Token): value is Token {
  return (
    value instanceof Token &&
    value.expiresIn !== undefined &&
    value.username !== undefined
  );
}
