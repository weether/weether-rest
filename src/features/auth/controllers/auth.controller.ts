import { Body, Controller, Post } from '@nestjs/common';
import { LoginCmd } from '../application/login-cmd';
import { SaveUserCmd } from '../../users/application/save-user-cmd';
import { UserDto } from '../../users/controllers/user-dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly loginCmd: LoginCmd,
    private readonly signUpCmd: SaveUserCmd,
  ) {}

  @Post('tokens')
  async login(@Body() user: UserDto) {
    return await this.loginCmd.run(user);
  }
}
