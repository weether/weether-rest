import { CanActivate, Injectable } from '@nestjs/common';
import { ExecutionContext } from '@nestjs/common/interfaces/features/execution-context.interface';
import { Observable } from 'rxjs';
import { UserOrmRepository } from '../../users/infraestructure/user.orm-repository';
import { JwtService } from '../application/jwt.service';

@Injectable()
export class AuthenticateUserGuard implements CanActivate {
  constructor(
    private readonly userRepository: UserOrmRepository,
    private readonly jwtService: JwtService,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const encodedToken = context.switchToHttp().getRequest();

    try {
      this.jwtService.verifyAppToken(encodedToken);
      return true;
    } catch (e) {
      return false;
    }
  }
}
