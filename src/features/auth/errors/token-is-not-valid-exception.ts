export class TokenIsNotValidException extends Error {
  constructor() {
    super('Token is not valid');
  }
}
