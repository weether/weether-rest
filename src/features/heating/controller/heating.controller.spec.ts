import { Test, TestingModule } from '@nestjs/testing';
import { HeatingController } from './heating.controller';

describe('Heating Controller', () => {
  let controller: HeatingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HeatingController],
    }).compile();

    controller = module.get<HeatingController>(HeatingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
