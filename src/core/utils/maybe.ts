import { CallbackFunction } from './callback-function';

export class Maybe<T> {
  private constructor(private value: T | null) {}

  static some<T>(value: T): Maybe<T> {
    if (!this.isValid(value)) {
      throw new Error('Provided value is empty');
    }
    return new Maybe(value);
  }

  static none<T>(): Maybe<T> {
    return new Maybe<T>(null);
  }

  static fromValue<T>(value: T | undefined | null): Maybe<T> {
    return this.isValid(value) ? Maybe.some(value as T) : Maybe.none<T>();
  }

  private static isValid(value: unknown | null | undefined): boolean {
    return (
      !!value ||
      this.isNumberZero(value) ||
      this.isFalse(value) ||
      this.isEmptyString(value)
    );
  }

  private static isNumberZero<R>(value: R): boolean {
    return typeof value === 'number' && value === 0;
  }

  private static isEmptyString<R>(value: R): boolean {
    return typeof value === 'string' && value === '';
  }

  private static isFalse<R>(value: R): boolean {
    return typeof value === 'boolean' && !value;
  }

  has(): boolean {
    return this.value !== null;
  }

  getOrElse(defaultValue: T): T {
    return this.value === null ? defaultValue : this.value;
  }

  getOrOther<R = T>(defaultValue: R): T | R {
    return this.value === null ? defaultValue : this.value;
  }

  getOrExecute(defaultValue: CallbackFunction<T>): T {
    return this.value === null ? defaultValue() : this.value;
  }

  map<R>(f: (wrapped: T) => R): Maybe<R> {
    if (this.value === null) {
      return Maybe.none<R>();
    } else {
      return Maybe.some(f(this.value));
    }
  }

  tap(f: (wrapped: T) => void): Maybe<T> {
    if (this.value !== null) {
      f(this.value);
    }

    return Maybe.fromValue(this.value);
  }

  flatMap<R>(f: (wrapped: T) => Maybe<R>): Maybe<R> {
    if (this.value === null) {
      return Maybe.none<R>();
    } else {
      return f(this.value);
    }
  }

  getOrThrow(error?: Error): T {
    return this.value === null
      ? (() => {
          if (error !== undefined) {
            throw error;
          }
          throw new Error('Value is empty');
        })()
      : this.value;
  }
}
