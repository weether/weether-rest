const ifaces = require('os').networkInterfaces();

export function getLocalIP(): string {
  let address = '';
  for (const dev in ifaces) {
    ifaces[dev].filter(details =>
      details.family === 'IPv4' &&
      details.internal === false &&
      details.netmask === '255.255.255.0'
        ? (address = details.address)
        : undefined,
    );
  }
  return address;
}
