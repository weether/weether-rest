type ArgumentTypes<T> = T extends (...args: infer U) => infer R ? U : never;
type ReplaceReturnType<TNewReturn> = (...a: ArgumentTypes<any>) => TNewReturn;
