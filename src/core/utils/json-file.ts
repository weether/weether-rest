import * as fs from 'fs';

export function readFromJSONFile(file): Record<string, any> {
  return JSON.parse(fs.readFileSync(file).toString());
}

export function truncateIntoJSONFile(json: Record<string, any>, file: string) {
  fs.writeFileSync(file, JSON.stringify(json));
}
