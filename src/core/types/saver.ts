export interface Saver<Resource> {
  save(resource: Resource): Promise<void>;
}
