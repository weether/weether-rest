export interface Adapter<Model, ModelDto> {
  toDto(model: Model): ModelDto;

  toModel(modelDto: ModelDto): Model;
}
