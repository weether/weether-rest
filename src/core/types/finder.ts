export interface Finder<Resource, Params = void> {
  find(params: Params): Promise<Resource>;
}
