export interface Query<Result = void, Params = void> {
  run(params: Params): Promise<Result>;
}
