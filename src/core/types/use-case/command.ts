export interface Command<Params = void> {
  run(params: Params): Promise<void>;
}
