/* eslint-disable @typescript-eslint/no-unused-vars */
import { Mapper } from './mapper.decorator';
import { Adapter } from '../types/adapter';

interface ModelWithDifferentPropName {
  foo: string;
  bar: number;
}
interface ModelWithDifferentPropNameDto {
  foo: string;
  barDto: number;
}
class ModelWithDifferentPropNameAdapter
  implements
    Adapter<ModelWithDifferentPropName, ModelWithDifferentPropNameDto> {
  @Mapper<ModelWithDifferentPropName, ModelWithDifferentPropNameDto>({
    bar: 'barDto',
  })
  toDto(
    model: ModelWithDifferentPropName,
  ): ModelWithDifferentPropNameDto {
    return;
  }

  @Mapper<ModelWithDifferentPropNameDto, ModelWithDifferentPropName>({
    barDto: 'bar',
  })
  toModel(
    model: ModelWithDifferentPropNameDto,
  ): ModelWithDifferentPropName {
    return;
  }
}

interface Model {
  foo: string;
  bar: number;
}
interface ModelDto {
  foo: string;
  bar: number;
}
class ModelAdapter implements Adapter<Model, ModelDto> {
  @Mapper()
  toDto(
    model: Model,
  ): ModelDto {
    return;
  }

  @Mapper()
  toModel(
    model: ModelDto,
  ): Model {
    return;
  }
}

describe('Adapter', () => {
  it('should return ModelDto with same property names as Model', async () => {
    const { modelAdapter: sut } = setup();
    const model: Model = { foo: 'im foo', bar: 4 };

    const actual = sut.toDto(model);

    expect(actual).toEqual(model);
  });

  it('should return model with changed property name', async () => {
    const { modelWithDifferentPropNameAdapter: sut } = setup();
    const model: ModelWithDifferentPropName = { foo: 'im foo', bar: 5 };

    const actual = sut.toDto(model);

    expect(actual).toEqual({ foo: 'im foo', barDto: 5 });
  });
});

function setup() {
  return {
    modelAdapter: new ModelAdapter(),
    modelWithDifferentPropNameAdapter: new ModelWithDifferentPropNameAdapter(),
  };
}
