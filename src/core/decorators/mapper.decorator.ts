export function Mapper<T, R>(mappings?: Partial<Record<keyof T, keyof R>>) {
  return function(target, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.value = function(...args: any[]) {
      if (args.length != 1) {
        throw Error(
          `Mapper annotation only supports 1 parameter. ${args.length} parameters where introduced.`,
        );
      }

      const objectToMap = args[0];
      const mappedObject = {};

      if (mappings === undefined) {
        return objectToMap;
      }

      Object.entries(mappings).forEach(([oldPropName, newPropName]) => {
        Object.assign(mappedObject, {
          [newPropName as string]: objectToMap[oldPropName],
        });
      });

      Object.entries(objectToMap)
        .filter(([key]) => !Object.keys(mappings).includes(key))
        .forEach(([key, value]) =>
          Object.assign(mappedObject, { [key]: value }),
        );

      return mappedObject;
    };
  };
}
