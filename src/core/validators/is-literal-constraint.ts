import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class IsLiteral implements ValidatorConstraintInterface {
  validate(word: string, args: ValidationArguments) {
    return args.constraints.includes(word);
  }

  defaultMessage(args: ValidationArguments) {
    return `Word ($value) does not meet string literal constraints (${args.constraints.map(
      x => x,
    )})`;
  }
}
