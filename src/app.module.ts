import { Module } from '@nestjs/common';
import { DbModule } from './db/db.module';
import { SnapshotModule } from './features/snapshot/snapshot.module';

@Module({
  imports: [SnapshotModule, DbModule],
})
export class AppModule {}
