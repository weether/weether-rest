import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { TypeValidationPipe } from './core/pipes/type-validation-pipe';
import { getLocalIP } from './core/utils/get-local-ip';
import { ConfigService } from './config/application/config.service';
import { Environment } from './config/domain/environment';
import { Logger } from '@nestjs/common';
import { LoggerTopics } from './core/types/logger-topics';

async function bootstrap() {
  if (ConfigService.getEnvironment() !== Environment.PRO_DOCKER) {
    Logger.log(
      `Local machine IP Address: ${getLocalIP()}`,
      LoggerTopics.SERVER,
    );
  }

  const app = await NestFactory.create<NestFastifyApplication>(AppModule, {
    // logger: false,
  });
  app.useGlobalPipes(new TypeValidationPipe());
  // TODO: Uncomment when postgres exception filter is implemented
  // app.useGlobalFilters(new MongoExceptionFilter());
  await app.listen(3000);
  Logger.log('Server listening on port 3000!', LoggerTopics.SERVER);
}

bootstrap();
